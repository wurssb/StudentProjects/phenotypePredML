"""
Script to: query HDT files with genomes using SPARQL to retrieve domain count information
// todo extend to make it easy to query different things (genes, domains, domain count etc.)
Returns: A pandas dataframe stored as a pickle containing the queried genome information. Rows are genbank accession numbers

Author: Kewin
"""

# import statements
import os.path
import pickle
import subprocess
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON


# functions
def query_genotype(input_folder, outname, geno_query):
    """Extract genotype information using a SPARQL query

    input_folder: str name of input folder containing .hdt files
    geno_query: str name of SPARQL query text file

    :return: csv file with selected SPARQL variables as columns
    """
    # return output if it already exists
    if os.path.exists(outname):
        return outname
    # if not, build the command and send it to the shell
    cmd = f"java -jar HDTQuery.jar -i {input_folder} -f csv -o {outname} -r -query {geno_query}"
    subprocess.check_call(cmd, shell=True)
    return outname


def convert_csv_to_pandas(infile):
    """Convert a csv file into a pandas dataframe with protein domain counts
    with gca as row names and protein domain as column names

    :param infile: str name of input csv with columns genbank number,
    protein domain and protein domain count
    """
    df = pd.read_csv(infile)
    # df.drop(df[(df[df.columns[0]] == df.columns[0])].index, inplace=True)
    # print(df.head())
    # print(df[1])
    df.rename(index=lambda sample: sample.split('/')[4], inplace=True)

    print(df.head())
    df.rename(columns={'sample': 'genbank'}, inplace=True)
    df = pd.pivot_table(df, values="accession_count", index="genbank", columns="accession",
                        aggfunc=max, fill_value=0)  # todo: duplicate GCA numbers: is max good fix?

    shape1 = df.shape[1]
    shared_df = df.loc[:, (df != 0).any(axis=0)]
    shape2 = shared_df.shape[1]
    print(shape1 - shape2, f"({(shape1 - shape2) / shape1 * 100:.0f}%) columns removed with only zeroes")
    return df


def retrieve_geno_df(input_folder, output_folder, geno_query):
    """Query a folder with hdt genome files to retrieve a pandas dataframe with
    gca number as row names, protein domain as column names & domain counts as values

    :param output_folder: str name of output folder where pickle and csv files will be stored
    :param geno_query: str name of SPARQL query text file
    :param input_folder: str name of folder with (subfolders with) hdt files
    Will also create a .csv and a .pickle if not existing
    """
    infile_name = input_folder.split("/")[-1]
    if os.path.exists(output_folder + infile_name + ".pickle"):
        with open(output_folder + infile_name + ".pickle", "rb") as pickle_in:
            geno_df = pickle.load(pickle_in)
    else:
        geno_csv = query_genotype(input_folder, output_folder + infile_name + ".csv", geno_query)
        geno_df = convert_csv_to_pandas(geno_csv)
        df_to_pickle(geno_df, output_folder + infile_name + ".pickle")
    return geno_df


def query_phenotype(pheno_query, endpoint, limit=0):
    """Extract phenotype information using a SPARQL query from the input endpoint

    pheno_query: str name of SPARQL query text file (from same dir)
    endpoint: str name of endpoint where the query is executed
    limit: int query limit

    :return: list of lists of [[Species, domain, domain count], -->]
    """
    with open(pheno_query, 'r') as f:
        query = f.read()
    if limit != 0:
        query += " limit " + str(limit)

    sparql = SPARQLWrapper(endpoint)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)  # csv doesn't work
    results = sparql.query().convert()
    return results


def parse_json_to_df(infile):
    """Return a dataframe from a SPARQL query in JSON format

    :param infile: str name of JSON file
    df: pandas DataFrame with first variable as index, rest as columns
    """
    # print(infile["head"]["vars"])
    # print(infile["results"]["bindings"])
    data_dict = {}
    var_list = infile["head"]["vars"]
    for var in var_list:
        # print(var)
        data_dict[var] = list(map(lambda res: res[var]["value"],
                                  infile["results"]["bindings"]))
    df = pd.DataFrame.from_dict(data_dict)
    df.set_index(df.columns[0], inplace=True)
    return df


def retrieve_pheno_df(input_query, output_folder, endpoint):
    infile_name = input_query.split("/")[-1][3:-4]
    if os.path.exists(output_folder + infile_name + ".pickle"):
        with open(output_folder + infile_name + ".pickle", "rb") as pickle_in:
            pheno_df = pickle.load(pickle_in)
    else:
        pheno_json = query_phenotype(input_query, endpoint, limit=0)
        pheno_df = parse_json_to_df(pheno_json)
        df_to_pickle(phenotype_df, output_folder + infile_name + ".pickle")
    return pheno_df


def df_to_pickle(pandas_df, pickle_path_name):
    """Convert a pandas DataFrame into a pickle

    :param pickle_path_name:
    :param pandas_df:
    :return:
    """
    if not os.path.exists(pickle_path_name + ".pickle"):
        print(f'Making pickle...')
        with open(pickle_path_name, "wb") as f:
            pickle.dump(pandas_df, f, pickle.HIGHEST_PROTOCOL)
        print("Done")
    return pickle_path_name


def find_intersection(geno_df, pheno_df):
    """Returns a pandas DataFrame with rows common between geno_df and pheno_df
    Returns None is there is no intersection. Empty columns (only zeroes) are removed

    :param geno_df: pandas dataframe with protein domain counts
    with gca as row names and protein domain as column names
    :param pheno_df: pandas dataframe with a column genbank accession
    numbers and a column with phenotype
    """
    intersection = set.intersection(set(pheno_df.index.values), set(geno_df.index.values))
    if intersection != set():
        shared_df = geno_df.loc[intersection]
        shared_df.insert(0, pheno_df.columns[0],
                         pheno_df.loc[intersection].iloc[:, 0], True)
        shape1 = shared_df.shape[1]
        shared_df = shared_df.loc[:, (shared_df != 0).any(axis=0)]
        shape2 = shared_df.shape[1]
        print(shape1 - shape2, f"({(shape1 - shape2) / shape1 * 100:.0f}%) columns removed with only zeroes")
        print(shared_df, "\n")
        return shared_df
    else:
        print("No intersection\n")
        return None


# def get_data_info(all_samples_info_df):
#     # Get amount of domains
#     for row in range(len(all_samples_info_df)):
#         non_zero_count = 0
#         for domain in all_samples_info_df.iloc[row]:
#             if domain != 0:
#                 non_zero_count += 1
#         print(f"{all_samples_info_df.iloc[row].name} "
#               f"has {100 * non_zero_count / len(all_samples_info_df.columns):.0f}% "
#               f"domains ({non_zero_count}/{len(all_samples_info_df.columns)})")
#     print(f"\nThere are {len(all_samples_info_df)} species of which "
#           f"{len(all_samples_info_df.index.unique())} unique")
#     print(f'There are {len(all_samples_info_df.columns)} domains')


# main

if __name__ == "__main__":
    geno_query_PATH = "SPARQL_queries/geno_queries/"
    pheno_query_PATH = "SPARQL_queries/pheno_queries/"
    data_PATH = "data/"
    HDT_PATH = "data/Bacteria/"
    endpoint = "http://sparql.systemsbiology.nl/proxy/wdqs/bigdata/namespace/wdq/sparql"

    df = convert_csv_to_pandas("data/all_genomes_10000.csv")
    print("df made")
    df_to_pickle(df, "data/all_genomes.pickle")
    print("Done")
    # # Geno query
    # genome_df = retrieve_geno_df(HDT_PATH + "big_group", data_PATH,
    #                              geno_query_PATH + "getGenotype.txt")
    #
    # # Pheno queries
    # for query in os.listdir(pheno_query_PATH):
    #     print("\nQuery:", query)
    #     phenotype_df = retrieve_pheno_df(pheno_query_PATH + query, data_PATH, endpoint)
    #
    #     # Intersection
    #     find_intersection(genome_df, phenotype_df)
