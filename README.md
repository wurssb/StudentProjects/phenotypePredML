# General Pipeline Setup
This pipeline allows for microbial phenotype classification using a variety of data processing methods and supervised classification methods, heavily relying on the [sklearn](scikit-learn.org) Python library.

This pipeline consists of 3 parts:
1. Input data is retrieved with [retrieving_input_data.ipynb](https://gitlab.com/wurssb/StudentProjects/phenotypePredML/-/blob/master/code/retrieving_input_data.ipynb)
2. The machine learning pipeline is done with [final_ml_pipeline](https://gitlab.com/wurssb/StudentProjects/phenotypePredML/-/blob/master/code/final_ml_pipeline.ipynb).
3. Results from 2 are visualized in [make_boxplots.ipynb](https://gitlab.com/wurssb/StudentProjects/phenotypePredML/-/blob/master/code/make_boxplots.ipynb)

For a more detailed description please check the individual scripts.


