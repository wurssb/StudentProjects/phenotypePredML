SELECT ?genbank ?name ?gramLabel
WHERE {
  ?entry wdt:P7 ?genbank ;
	 wdt:P50 ?name ;
         wdt:P32 ?gram ;
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
